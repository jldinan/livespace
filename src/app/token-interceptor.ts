import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  getToken() : string {
    return 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJDbGllbnRJZCI6MSwiRG9tYWluIjoiMTAuMC4wLjE5MSIsIkRpc3BsYXlJZCI6bnVsbCwiRGlzcGxheU5hbWUiOm51bGwsIkdyb3VwcyI6bnVsbCwiSGlkZU9uRW1wdHlDb250ZW50IjpmYWxzZSwiVXNlcm5hbWUiOiJkZWZhdWx0IiwiRXhwIjoxNTMzMTMyMTc5fQ.S5DsHqzPvqYerAKO5hIJAbz6oNtqfJenQEeBj0DBo1U';
  }

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({
      setHeaders: {
        'X-Auth': `${this.getToken()}`
      }
    });

    return next.handle(request);
  }
}
