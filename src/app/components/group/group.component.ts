import { Component, OnInit } from '@angular/core';
import { GroupService } from '../../services/group.service';
import { Group } from '../../services/group';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})

export class GroupComponent implements OnInit {

  constructor(private groupService: GroupService) { }

  public groups: Group[];

  ngOnInit() {
    this.getGroups();
  }

  getGroups(): void {
    this.groupService.getGroups()
      .subscribe((result: Group[]) => this.groups = result);
  }

  getGroupThumbnail(group: Group): string {
    let url: string = 'assets/thumbnails/';
    if (group.ScheduleItemsNow.length > 0) {
      url += group.ScheduleItemsNow[0].PageId + '.jpg';
    } else {
      url += 'blank.png';
    }
    return url;
  }
}
