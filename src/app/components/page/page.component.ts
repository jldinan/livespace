import { Component, OnInit } from '@angular/core';
import { PageService } from '../../services/page.service';
import { Page } from '../../services/page';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})

export class PageComponent implements OnInit {

  constructor(private pageService: PageService) { }

  public pages: Page[];
  private rootFolder : string = 'pages';
  private currentPath: string[] = [];

  ngOnInit() {
    this.getPagesByFolder(this.rootFolder, true);
  }

  getPagesByFolder(folder: string, addPath: boolean): void {
    if (addPath) {
      this.currentPath.push(folder);
    }
    this.pageService.getPagesByFolder(folder)
      .subscribe((result: Page[]) => this.pages = result);
  }

  getPageThumbnail(page: Page): string {
    let url: string = 'assets/thumbnails/';
    url += page.Id + '.jpg';
    return url;
  }

  moveUpFolder(): void {
    if (this.currentPath.length > 1) {
      this.currentPath.pop();
      this.getPagesByFolder(this.currentPath[this.currentPath.length - 1], false);
    }
  }
}
