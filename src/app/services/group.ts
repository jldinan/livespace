export class Group {
  Id: number;
  Name: string;
  Description: string;
  ScheduleItemsNow: Array<ScheduleItemsNow>;
}

export class ScheduleItemsNow {
  PageId: number;
}
