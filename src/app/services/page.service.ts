import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Page } from './page';

@Injectable({
  providedIn: 'root'
})

export class PageService {

  constructor(private http: HttpClient) { }

  getPages(): Observable<Page[]> {
    return this.http.get<Page[]>('/api/sign/pages');
  };

  getPagesByFolder(folder: string): Observable<Page[]> {
    return this.http.get<Page[]>('/api/sign/pages', {
      params: {
        folder: folder
      }
    });
  };
}
