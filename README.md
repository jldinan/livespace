# LiveSpace V2
LiveSpace UI Created with latest version of Angular (6.x)
Connects to existing Manager API endpoints. 

To run locally, in the package.json change the start script:

```
"start": "ng serve --host 10.0.0.244 --proxy-config proxy.conf.json"
```

To point to your own host machine. Then cd to the directory in a node prompt & run:

```
npm start
```

After initial compile the UI will be available at your host IP on port 4200. All file changes are watched and will be updated live in the browser.
